import React, { Component } from 'react';
import {
  StyleSheet,   // CSS-like styles
  Text,         // Renders text
  View,
  Image          // Container component
} from 'react-native';

import { Card, ListItem, Button } from 'react-native-elements'

const FeedCard = ({cardHeader, cardTitle, cardText, cardType}) => {
  return(
      <Card title="Song">
        <View style={styles.block}>
          <Image
            style={{ width: 250, height: 250 }}
            resizeMode="cover"
            source={{ uri: 'https://scontent-sit4-1.xx.fbcdn.net/v/t1.0-9/21686084_1647818201956227_1688569012924868176_n.jpg?oh=56b867a1632b7d77532cf19abd0d9491&oe=5AFB50C7' }}
          />
          <Text style={styles.name}>{'Shiva'}</Text>
        </View>
      </Card>
    )
}

const styles = StyleSheet.create({
  headline: {
    alignItems: 'center'
  },

  headlineText: {
    color: '#FFFFFF',                   // White color
    fontFamily: 'sans-serif-condensed'
  },
  // App container
  container: {
    flex: 1,                            // Take up all screen
    backgroundColor: '#354953',         // Background color
  },
  // Tab content container
  content: {
    flex: 1,                            // Take up all available space
    justifyContent: 'center',           // Center vertically
    alignItems: 'center',               // Center horizontally
    backgroundColor: '#5E8392',         // Darker background for content area
  },
  // Content header
  header: {
    margin: 10,                         // Add margin
    color: '#FFFFFF',                   // White color
    fontSize: 26,                       // Bigger font size
  },
  // Content text
  text: {
    marginHorizontal: 20,
    color: 'rgba(255, 255, 255, 0.75)', // Semi-transparent text
    textAlign: 'center',                // Center
    fontSize: 18,
  },
});


export default FeedCard
