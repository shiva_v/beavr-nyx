import React, { Component } from 'react'
import {
  StyleSheet,   // CSS-like styles
  Text,         // Renders text
  View,
  Image,
  ActivityIndicator          // Container component
} from 'react-native'
import { Icon } from 'react-native-elements'

import * as firebase from 'firebase'

import FeedCard from './components/feedCard'
import Tabs from './components/tabs'
import { Button } from './components/button'
import PlayButton from './components/playButton'
import { Input } from './components/input'

const baseURL = 'http://192.168.0.51:3000/'

export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loggedIn: false,
      email: '',
      password: '',
      authenticating: false
    }
    this.onPressLogIn = this.onPressLogIn.bind(this)
  }

  componentDidMount() {
    fetch(baseURL + 'v1/genres/all')
      .then((response) => {
        console.log(response._bodyText)
      })
  }

  componentWillMount() {
    const firebaseConfig = {
      apiKey: 'AIzaSyCNFnrH57WX9LhapdgT1ltt_LVxI3k8tns',
      authDomain: 'beavr-b428c.firebaseapp.com',
    }
    firebase.initializeApp(firebaseConfig)
  }

  onPressLogIn() {
    firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password).catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      console.log(errorMessage + ' error')
      // ...
    }).then(() => {
      // Successfully logged in
      var user = firebase.auth().currentUser;

      if (user) {
        // User is signed in.
        console.log(user)
      } else {
        // No user is signed in.
      }
    })
    this.setState({
      loggedIn: true,
    })
  }

  render() {
    if(!this.state.loggedIn) {
      return(
        <View style={styles.loginContainer}>
          <Icon
            raised
            name='music'
            type='font-awesome'
            color='blue'
          />
          <Input
            label='Email'
            placeholder='Enter your email'
            onChangeText={email => this.setState({ email })}
          />
          <Input
            label='Password'
            placeholder='Enter your password'
            secureTextEntry
            onChangeText={password => this.setState({ password })}
          />
          <Button
            onPress={() => console.log('Pressed login') }
            children={'Log In'}
            onPress={() => this.onPressLogIn()}
          />
        </View>
      )
    } else {
      return (
        <View style={styles.container}>
          <Tabs>
            {/* First tab */}
            <View title="FEED" style={styles.content}>
              <Icon
                reverse
                name='home'
                type='octicon'
                color='#517fa4'
              />
              <Text style={styles.header}>
                Welcome to beavr
              </Text>
              <Text style={styles.text}>
                Play me some tunes beavr!
              </Text>
              <FeedCard  />
            </View>
            {/* Second tab */}
            <View title="DISCOVER" style={styles.content}>
              <Text style={styles.header}>
                Hot right now!
              </Text>
              <Text style={styles.text}>
                Fresh off the oven.
              </Text>
            </View>
            {/* Third tab */}
            <View title="FAVORITES" style={styles.content}>
              <Text style={styles.header}>
                Discover the latest from the indie universe!
              </Text>
              <Text style={styles.text}>
                Shazam that tune for more information.
              </Text>
              <PlayButton pressHandler={()=> { console.log("Pressed") }}/>
            </View>

          </Tabs>
        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  loginContainer: {
    flex: 1,
    padding: 20,
  },
  headline: {
    alignItems: 'center'
  },

  headlineText: {
    color: '#FFFFFF',                   // White color
    fontFamily: 'notoserif'
  },
  // App container
  container: {
    flex: 1,
    backgroundColor: '#00D2D5'
  },
  // Tab content container
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff'
  },
  // Content header
  header: {
    margin: 10,
    color: '#000000',
    fontSize: 26,
  },
  // Content text
  text: {
    marginHorizontal: 20,
    color: '#000000',
    textAlign: 'center',
    fontSize: 18,
  },
  LoginButton: {
    backgroundColor: 'lightcoral'
  }
});
