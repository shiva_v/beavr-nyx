import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native'
import { Icon } from 'react-native-elements'

import FeedCard from '../components/feedCard'

export default class Feed extends Component {
  render() {
    return(
      <View title="WELCOME" style={styles.content}>
        <Icon
          reverse
          name='home'
          type='octicon'
          color='#517fa4'
        />
        <Text style={styles.header}>
          Welcome to beavr
        </Text>
        <Text style={styles.text}>
          The best technology to build cross platform mobile apps with
        </Text>
        <FeedCard />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,                            // Take up all available space
    justifyContent: 'center',           // Center vertically
    alignItems: 'center',               // Center horizontally
    backgroundColor: '#5E8392'          // Darker background for content area
  },
  // Content header
  header: {
    margin: 10,                         // Add margin
    color: '#FFFFFF',                   // White color
    fontSize: 26,                       // Bigger font size
  },
  // Content text
  text: {
    marginHorizontal: 20,
    color: 'rgba(255, 255, 255, 0.75)', // Semi-transparent text
    textAlign: 'center',                // Center
    fontSize: 18,
  },
});
